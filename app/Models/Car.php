<?php

namespace App\Models; //Autoload -> archivo que maneja la ubicacion de todos los archivos dentro el proyecto

use Illuminate\Database\Eloquent\Factories\HasFactory; //facades codigo inyectado en archivo
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //PHP -> lenguaje BAckend -> debilmente tipado
    //se ejecuta de lado del servidor
        // Variable -> iniciar con un tipo valor y terminar con otro valor

    use HasFactory; // trait ->

    protected $table = 'car';
    protected $primaryKey = 'id_car';
    public $timestamps = true;

    protected $fillable =[
        'year_car',
        'id_car_model',
        'id_car_make'
    ];
}
