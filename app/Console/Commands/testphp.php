<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class testphp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        //Variables
        $var_integer = 1; //sencillas
        $var_string = 'string'; //sencillas
        $var_array = [
            'val1'=>1,  //'name' => 'value
            [ // niveles 2
                'val2'=> 3
            ]
        ];

        // condicionales
        if ($var_integer >= 2){ //pregunta Yes/No
            echo 'Hola mundo';
        }else{
            echo 'es menor';
        }
        $var_collection = collect(); //obj => json Modelo Eloquent
    }
}
