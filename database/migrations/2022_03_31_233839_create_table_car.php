<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car', function (Blueprint $table) {
            //$table "->" se usa para acceder a atributos dentro el objeto
            $table->engine ='InnoDB'; //deadlock -waitlock // engine -> Myisam ///veloz
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            //create table test (id integer not null Auto_increment, year_car integer not null Primary key(id)) slq basico
            $table->increments('id_car');
            $table->integer('year_car')->nullable();
            $table->integer('id_car_make')->nullable();
            $table->integer('id_car_model')->nullable();
            $table->timestamps(); // 2 campos created_at, updated_at date
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car');
    }
};
