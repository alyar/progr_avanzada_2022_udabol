<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engine', function (Blueprint $table) {
            $table->engine = 'InnoDB'; //secure execution -> DeadLock -> 2 o mas recursos se utilizan al mismo tiempo
            $table->charset = 'utf8'; //""
            $table->collation = "utf8_unicode_ci";
            $table->increments('id_engine');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engine');
    }
};
